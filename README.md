# Url Shortener 
This is a simple url shortener written in Python for recruitment purposes.


## Requirements
- Docker and Docker Compose
- Python 3.11
- FastAPI
- Pytest

## Getting Started
1. Clone the repository.
2. Navigate to the project directory.
3. Use docker-compose to build and start the container:
    ```bash
    docker-compose up --build
    ```
4. The application will be available at `http://localhost:8008`.

## Code quality
### Setting up pre-commit

1. (Optional) Create a virtual environment with `python -m venv venv` and activate it with `source venv/bin/activate` on Unix or `venv\Scripts\activate` on Windows. 
2. Install pre-commit with `pip install pre-commit`.
3. Run `pre-commit install` in the root directory of the repository.

## Application structure
```
.
├── .gitignore
├── .pre-commit-config.yaml
├── README.md
├── backend
│   ├── Dockerfile
│   ├── app
│   │   ├── __init__.py
│   │   ├── main.py
│   │   ├── schemas.py
│   │   ├── test_main.py
│   │   └── utils.py
│   ├── requirements-test.txt
│   └── requirements.txt
├── docker-compose.yaml
├── frontend
└── pyproject.toml

```

## API Endpoints
- **/encode/**: Encodes a URL to a shortened version. Returns JSON. Requires a JSON body with a `url` key.
- **/decode/{shortened}**: Decodes a shortened URL to its original URL. Returns JSON.

## Tests
To run the tests, follow these steps:
1. Activate your virtual environment.
2. Install the test requirements with `pip install -r backend/requirements-test.txt`.
3. Run the tests with `pytest backend/app/test_main.py`.

## Frontend
To start the frontend, navigate to the `frontend` directory and run `npm install` to install the dependencies. Then, run `npm run start`. The frontend will be available at `http://localhost:3000`.
