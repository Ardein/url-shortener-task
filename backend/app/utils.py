import random
import string


def shorten_url() -> str:
    return "".join(
        random.choices(
            string.ascii_letters + string.digits,
            k=6,
        ),
    )
