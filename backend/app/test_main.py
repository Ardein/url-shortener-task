from unittest.mock import patch

from fastapi.testclient import TestClient

from .main import app

client = TestClient(app)


def test_encode():
    with patch("app.database.urls") as mock_urls:
        mock_urls.insert_one.return_value = None
        mock_urls.find_one.return_value = {
            "url": "https://www.markant.com",
            "shortened": "shortened_url",
        }

        response = client.post(
            "/encode/",
            json={"url": "https://www.markant.com"},
        )
        assert response.status_code == 200
        assert "shortened" in response.json()
        assert isinstance(response.json()["shortened"], str)


def test_decode_valid():
    with patch("app.database.urls") as mock_urls:
        mock_urls.find_one.return_value = {
            "url": "https://www.markant.com",
        }

        response = client.get(
            "/decode/shortened_url",
        )
        assert response.status_code == 200
        assert response.json() == {"url": "https://www.markant.com"}


def test_encode_exists():
    with patch("app.database.urls") as mock_urls:
        mock_urls.find_one.return_value = {
            "url": "https://www.markant.com",
            "shortened": "shortened_url",
        }

        response = client.post(
            "/encode/",
            json={"url": "https://www.markant.com"},
        )
        assert response.status_code == 200
        assert response.json() == {
            "url": "https://www.markant.com",
            "shortened": "shortened_url",
        }


def test_decode_not_found():
    with patch("app.database.urls") as mock_urls:
        mock_urls.find_one.return_value = None

        response = client.get(
            "/decode/shortened_url",
        )
        assert response.status_code == 404
        assert response.json() == {"detail": "URL not found"}
