from pymongo import MongoClient

client = MongoClient("mongo", 27017)

db = client["url-shortener"]
urls = db["urls"]


def insert_url(url: str, shortened: str) -> None:
    urls.insert_one({"url": url, "shortened": shortened})


def get_shortened_url(full_url: str) -> str | None:
    url = urls.find_one({"url": full_url})
    return url["shortened"] if url else None


def get_url(shortened: str) -> str | None:
    url = urls.find_one({"shortened": shortened})
    return url["url"] if url else None
