import re

from pydantic import BaseModel, field_validator


class BaseUrl(BaseModel):
    url: str

    @field_validator("url")
    @classmethod
    def validate_url(cls, value: str) -> str:
        pattern = r"http[s]?://\S+"
        if not re.match(pattern, value):
            raise ValueError("Invalid URL")
        return value


class UrlIn(BaseUrl):
    pass


class UrlOut(BaseUrl):
    shortened: str
