from fastapi import FastAPI, HTTPException, status
from fastapi.middleware.cors import CORSMiddleware

from .database import get_shortened_url, get_url, insert_url
from .schemas import BaseUrl, UrlIn, UrlOut
from .utils import shorten_url

app = FastAPI()

origins = [
    "http://localhost:3000",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.post(
    "/encode/",
    response_model=UrlOut,
    status_code=status.HTTP_200_OK,
)
async def encode(url: UrlIn) -> UrlOut:
    exists = get_shortened_url(url.url)

    # If the URL already exists, return the existing shortened URL
    if exists is not None:
        return UrlOut(url=url.url, shortened=exists)

    # Ensure the shortened URL is unique - so we don't overwrite existing URLs
    while get_url(shortened := shorten_url()):
        pass

    insert_url(url.url, shortened)
    return UrlOut(url=url.url, shortened=shortened)


@app.get(
    "/decode/{shortened}",
    response_model=BaseUrl,
    status_code=status.HTTP_200_OK,
)
async def decode(shortened: str) -> BaseUrl:
    url = get_url(shortened)

    if url is not None:
        return BaseUrl(url=url)

    raise HTTPException(
        status_code=status.HTTP_404_NOT_FOUND,
        detail="URL not found",
    )
