import React from "react";

import { Button, Container, Form, InputGroup, Row } from "react-bootstrap";

import { useApp } from "./useApp";

function App() {
  const { url, setUrl, shortUrl, setShortUrl, encodeUrl, decodeUrl } = useApp();

  return (
    <Container className="App">
      <Form>
        <Form.Group as={Row} className="justify-content-md-center mt-2 ">
          <Form.Label htmlFor="basic-url">URL</Form.Label>
          <InputGroup>
            <Form.Control
              id="basic-url"
              value={url}
              onChange={(event) => setUrl(event.currentTarget.value)}
            />
            <Button onClick={encodeUrl}>Shorten</Button>
          </InputGroup>
        </Form.Group>

        <Form.Group as={Row} className="justify-content-md-center mt-2 ">
          <Form.Label htmlFor="basic-url">Short URL</Form.Label>
          <InputGroup>
            <InputGroup.Text>http://localhost:8008/decode/</InputGroup.Text>
            <Form.Control
              id="short-url"
              value={shortUrl}
              onChange={(event) => setShortUrl(event.currentTarget.value)}
            />
            <Button onClick={decodeUrl}>Decode</Button>
          </InputGroup>
        </Form.Group>
      </Form>
    </Container>
  );
}

export default App;
