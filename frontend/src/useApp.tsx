import { useState } from "react";

import axios, { AxiosError } from "axios";

export const useApp = () => {
  const [url, setUrl] = useState("");
  const [shortUrl, setShortUrl] = useState("");

  const encodeUrl = async () => {
    try {
      const response = await axios.post("http://localhost:8008/encode/", {
        url,
      });
      setShortUrl(response.data.shortened);
    } catch (error) {
      const err = error as AxiosError;
      if (err.response?.status === 422) {
        setShortUrl("Invalid URL");
      } else {
        setShortUrl("An error occurred while shortening the URL");
      }
    }
  };

  const decodeUrl = async () => {
    try {
      const response = await axios.get(
        `http://localhost:8008/decode/${shortUrl}`,
      );
      setUrl(response.data.url);
    } catch (error) {
      const err = error as AxiosError;
      if (err.response?.status === 404) {
        setUrl("No matching URL found for the given shortened URL.");
      } else {
        setUrl("An error occurred while decoding the URL");
      }
    }
  };

  return { url, setUrl, shortUrl, setShortUrl, encodeUrl, decodeUrl };
};
